#!/bin/bash

# This script will generte the files for a maven repository
# in ../maven_repo/repository folder


path=$(pwd)
sourcePath="$(dirname "$(pwd)")/virtuoso/"
repoPath="$(dirname "$(pwd)")/maven_repo"

echo -e "Enter the source path for pom.xml (default: $sourcePath): \c"
read pomPath

if [ "${pomPath}" != "" ]; then
  if [ ${pomPath: -1} != "/" ]; then
    sourcePath="${pomPath}/"
  else
    sourcePath="${pomPath}"
  fi
fi

pomFile="${sourcePath}pom.xml"

# Check that the pom.xml file exists
if [ ! -f $pomFile ]; then
  echo "Unable to open $pomFile"
  return	
fi  

# Read the pom.xml for tags
DgroupId=$(grep -oPm1 "(?<=<groupId>)[^<]+" $pomFile)
artifactId=$(grep -oPm1 "(?<=<artifactId>)[^<]+" $pomFile)
version=$(grep -oPm1 "(?<=<version>)[^<]+" $pomFile)
#name=$(grep -oPm1 "(?<=<name>)[^<]+" pom.xml)

# Ask if different inputs should be used
echo "Default values are taken from $pomFile"
echo -e "Enter DartifactId (default: $artifactId): \c "
read DartifactId
echo -e "Enter Dversion (default: $version): \c "
read Dversion
echo -e "Enter Repository Path (default: $repoPath): \c "
read DrepoPath

# Checks if reads were blank and if true inserts defaults
if [${DartifactId} = ""]; then
  DartifactId=${artifactId}
fi

if [${Dversion} = ""]; then
  Dversion=${version}
fi

if [${DrepoPath} = ""]; then
  DrepoPath="${repoPath}"
fi


# Check $DrepoPath ends with /
if [ ${DrepoPath: -1} != "/" ]; then
  DrepoPathChecked="${DrepoPath}/"
else
  DrepoPathChecked=${DrepoPath}
fi

# Create file names
Dfile="dist/$DartifactId-$Dversion.jar"
Djavadoc="dist/$DartifactId-$Dversion-javadoc.jar"
Dsources="dist/$DartifactId-$Dversion-sources.jar"
DpomFile="pom.xml"
DlocalRepositoryPath="$DrepoPathChecked""repository"
echo $DlocalRepositoryPath
# Create mvn command
mvn_command="mvn install:install-file -DgroupId=$DgroupId -DartifactId=$DartifactId -Dversion=$Dversion -Dfile=$Dfile -Djavadoc=$Djavadoc -Dsources=$Dsources -Dpackaging=jar -DpomFile=$DpomFile -DlocalRepositoryPath=$DlocalRepositoryPath -DcreateChecksum=true"

# Checks correct variables with user
echo
echo "This will run the mvn install with"
echo "DgroupId= $DgroupId"
echo "DartifactId= $DartifactId"
echo "Dversion= $Dversion"
echo "Dfile= $Dfile"
echo "Djavadoc= $Djavadoc"
echo "Dsources= $Dsources"
echo "DpomFile= $DpomFile"
echo "DlocalRepositortyPath = $DrepoPathChecked"
echo
echo -e "Enter 'Y' to confirm: \c "
read confirmation

# Executes mvn or exits
if [ -n "$confirmation" ]; then
  if [ ${confirmation^^} = "Y" ]; then
    echo
    echo "Changing directory to $sourcePath"
    cd $sourcePath
    echo "Running mvn"
    #$echo "${mvn_command}"
    $mvn_command
    echo "Changing directory to $repoPath"
    cd $repoPath
  else
    echo
    echo "Exiting"
  fi
else
  echo 
  echo "Exiting"
fi
echo
