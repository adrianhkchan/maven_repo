To add to new compiled versions of the Framwork to the GitLab tdcs_framework/maven_repo repository -

Compile the source locally

Run `. virtuoso_maven_repo.sh` to copy the source jar files into the repository folder. This will read the default values from the virtuoso/pom.xml which can be overridden if necessary, change to the source folder and execute the `mvn install` command generated and then change back to the repository folder.

For example, Virtuoso 1.4.0.1 was build in /home/user/framework/virtusoso and the default generated mvn command when executed from /home/user/framework/maven_repo is

```
mvn install:install-file -DgroupId=uk.gov.homeoffice
-DartifactId=virtuoso -Dversion=1.4.0.1
-Dfile=dist/virtuoso-1.4.0.1.jar
-Djavadoc=dist/virtuoso-1.4.0.1-javadoc.jar
-Dsources=dist/virtuoso-1.4.0.1-sources.jar
-Dpackaging=jar
-DpomFile=pom.xml
-DlocalRepositoryPath=/home/user/framework/maven_repo/repository
-DcreateChecksum=true
```

Add the new files with `git add .`
Commit the files to the repository with the version `git commit -m "Virtuoso 1.4.0.1"`
Push the files to the repository with `git push`
